//	Указывает пакет "pack", в котором находится файл класса
package rent;

/**
 *
 * @author пк
 */
public class Rent {

    /**
     * @param args the command line arguments
     */
    // Точка входа в программу
    public static void main(String[] args) {
        // Создание и вывод главной формы
        MainForm mf = new MainForm();
        mf.show();
    }
    
}
