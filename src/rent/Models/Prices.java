// Указывает пакет "pack", в котором находится файл класса
package rent.Models;

// Модель тарифов
public class Prices 
{
    // Объявление закрытых переменных
    private float priceWaterHot = 110,
                    priceWaterCold = 20,
                    priceElectric = 2.6f,
                    priceGaz = 5,
                    priceKeep = 17,
                    priceOverhaul = 7;
    
    // Свойства
    public float getPriceWaterHot() {return priceWaterHot;}
    public void setPriceWaterHot(float price) { priceWaterHot = price; }
    
    public float getPriceWaterCold() {return priceWaterCold;}
    public void setPriceWaterCold(float price) { priceWaterCold = price; }
    
    public float getPriceElectric() {return priceElectric;}
    public void setPriceElectric(float price) { priceElectric = price; }
    
    public float getPriceGaz() {return priceGaz;}
    public void setPriceGaz(float price) { priceGaz = price; }
    
    public float getPriceKeep() {return priceKeep;}
    public void setPriceKeep(float price) { priceKeep = price; }
    
    public float getPriceOverhaul() {return priceOverhaul;}
    public void setPriceOverhaul(float price) { priceOverhaul = price; }
    
    // --------
    
}
