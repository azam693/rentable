// Указывает пакет "pack", в котором находится файл класса
package rent.Models;

// Модель показателей счётчиков
public class PerformIndicators 
{
    private float waterHot, 
                    waterCold,
                    electric,
                    gaz,
                    keep,
                    overhaul;
    
    // Гетерыи и сетеры
    public float getWaterHot() {return waterHot;}
    public void setWaterHot(float price) { waterHot = price; }
    
    public float getWaterCold() {return waterCold;}
    public void setWaterCold(float price) { waterCold = price; }
    
    public float getElectric() {return electric;}
    public void setElectric(float price) { electric = price; }
    
    public float getGaz() {return gaz;}
    public void setGaz(float price) { gaz = price; }
    
    public float getKeep() {return keep;}
    public void setKeep(float price) { keep = price; }
    
    public float getOverhaul() {return overhaul;}
    public void setOverhaul(float price) { overhaul = price; }
}
