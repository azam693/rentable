//	Указывает пакет "pack", в котором находится файл класса
package rent;

//	Импортируются все классы из пространства пакета Handlers
import rent.Handlers.*;
//	Импортируются все классы из пространства пакета Models
import rent.Models.*;
/*	Импортирутся библиотека графического интерфейса,
"*" - подключение всех элементов библиотеки Swing.Table
*/
import javax.swing.table.*;

/*	
 * public - модификатор доступа, отвечающий за доступ, члены класса доступны всем
	В данном случае он означает что класс MainForm виден и доступен любому другому классу.
	Зарезервированное слово class означает описание класса MainForm.
 * Класс наследуется от элемента JFrame, соответсвенно все его методы также наследуются. JFrame содержит в себе всё необходимое для создания и функционирования окна программы.
  * extends это ключевое слово, предназначенное для расширения реализации какого-то существующего класса.
*/
public class MainForm extends javax.swing.JFrame {

    // Объявление закрытой переменной prices
    private Prices prices;
	// Объявление закрытой переменной rc
    private RentCalculate rc;
    
    /*
     * Конструктор класса MainForm
     */
    public MainForm() {
        
        // Создание экземпляра класса Prices
        prices = new Prices();
        
        /*
         * Создание экземпляра класса RentCalculate,
         * в качестве аргумента передаётся экземпляр класса Prices
         */
        rc = new RentCalculate(prices);
        
        // Вызов метода инициализации компонентов формы
        initComponents();
    }

    /*
     * Инициализация компонентов формы, задание базовых свойств,
     * компоновка формы
     * аннотация SuppressWarnings пишется когда Вы точно уверены, что участок кода безопасен 
     * для выполнения, но среда разработки все равно показывает предупреждения
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
		/*
		* Создание объектов графиского интерфейса формы
		* JLabel (метка) — предназначен для описания (текстового или графического) других элементов.
		* JTextField (текстовое поле) — позволяет пользователю вводить текстовые данные, которые могут быть обработаны в программе.
		*  JPanel - контейнер элементов
		* Обеспечивает представление с возможностью прокрутки легкого компонента. A JScrollPane управляет областью просмотра, дополнительной вертикалью и горизонтальными полосами прокрутки, и дополнительной строкой и областями просмотра заголовка столбца.
		* JTable выводит на экран (с возможностью отредактирования) регулярные двумерные таблицы ячеек.
		* JButton (кнопка) — основной активный компонент, позволяющий выполнить какие-либо действия при ее нажатии.
		*/
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        tWaterHot = new javax.swing.JTextField();
        tWaterCold = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        tElectric = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        tKeep = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        tGaz = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        tOverhaul = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        bTariff = new javax.swing.JButton();
        bCalculate = new javax.swing.JButton();
        lCalculate = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tResult = new javax.swing.JTable();
        bClear = new javax.swing.JButton();
		// ---------------------------------

		// Задание поведение для окна при закрытии
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
		// Метод устанавливает заголовок окна
        setTitle("Расчёт квартплаты");
		// Метод устанавливает фон окна
        setBackground(new java.awt.Color(204, 204, 204));
		// Метод устанавливает возможность изменения размеров окна
        setResizable(false);

		/*
		* В данном блоке задаются рамки для панели
		* Метод createTitledBorder задаёт заголовок для бордера
		*/
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Расчётные данные", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 2, 14), new java.awt.Color(255, 153, 51))); // NOI18N
        jPanel1.setForeground(new java.awt.Color(255, 153, 102));
        jPanel1.setToolTipText("");

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Вода", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(102, 102, 255))); // NOI18N
		// -------------------------------------------

		// задание имени компонентам JTextField
        tWaterHot.setName("tWaterHot"); // NOI18N

        tWaterCold.setName("tWaterCold"); // NOI18N
		// --------------------------------------

		// Задание параметров (Текст, размер шрифта) для компонента JLabel
        jLabel4.setText("Горячая вода:");

        jLabel5.setText("Холодная вода:");

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel3.setText("м3");

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel2.setText("м3");
		// ------------------------------------

		/*
		 * Создание объекта GroupLayout
		 * GroupLayout менеджер имеет возможность независимо устанавливать горизонтальное и вертикальное расположение компонентов на форме.
		 */
        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
		
		// Метод setLayout созланную панель в контейнер
        jPanel2.setLayout(jPanel2Layout);
		
		// Методы setHorizontalGroup и setVerticalGroup располагают элементы по осям x и y
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(tWaterHot, javax.swing.GroupLayout.DEFAULT_SIZE, 151, Short.MAX_VALUE)
                    .addComponent(tWaterCold))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tWaterHot, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(tWaterCold, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel5))
                    .addComponent(jLabel2))
                .addContainerGap(14, Short.MAX_VALUE))
        );
		// --------------------------

		/*
		* В данном блоке задаются рамки для панели
		* Метод createTitledBorder задаёт заголовок для бордера
		*/
        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Электричество", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(102, 102, 255))); // NOI18N
        jPanel3.setPreferredSize(new java.awt.Dimension(185, 101));
		// -------------------------------
		
		
        tElectric.setName("tElectric"); // NOI18N

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel1.setText("кВт");

		/*
		 * Создание объекта GroupLayout
		 * GroupLayout менеджер имеет возможность независимо устанавливать горизонтальное и вертикальное расположение компонентов на форме.
		 */
        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
		
		// Метод setLayout созланную панель в контейнер
        jPanel3.setLayout(jPanel3Layout);
		
		// Методы setHorizontalGroup и setVerticalGroup располагают элементы по осям x и y
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(tElectric, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel6)
                        .addGap(176, 176, 176))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel1)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(tElectric, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel1))
                    .addComponent(jLabel6))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
		// --------------------------------

		/*
		* В данном блоке задаются рамки для панели
		* Метод createTitledBorder задаёт заголовок для бордера
		*/
        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Текущий ремонт", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(102, 102, 255))); // NOI18N
        jPanel4.setPreferredSize(new java.awt.Dimension(185, 101));
		// ---------------------------

        tKeep.setName("tKeep"); // NOI18N

        jLabel9.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel9.setText("м2");

		/*
		 * Создание объекта GroupLayout
		 * GroupLayout менеджер имеет возможность независимо устанавливать горизонтальное и вертикальное расположение компонентов на форме.
		 */
        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
		// Метод setLayout созланную панель в контейнер
        jPanel4.setLayout(jPanel4Layout);
		// Методы setHorizontalGroup и setVerticalGroup располагают элементы по осям x и y
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(tKeep, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel9)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tKeep, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
		// ----------------------------------

		/*
		* В данном блоке задаются рамки для панели
		* Метод createTitledBorder задаёт заголовок для бордера
		*/
        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Газ", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(102, 102, 255))); // NOI18N
        jPanel5.setPreferredSize(new java.awt.Dimension(185, 101));
		// -------------------------------

        tGaz.setName("tGaz"); // NOI18N

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel7.setText("м3");

		/*
		 * Создание объекта GroupLayout
		 * GroupLayout менеджер имеет возможность независимо устанавливать горизонтальное и вертикальное расположение компонентов на форме.
		 */
        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
		// Метод setLayout созланную панель в контейнер
        jPanel5.setLayout(jPanel5Layout);
		// Методы setHorizontalGroup и setVerticalGroup располагают элементы по осям x и y
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(tGaz, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel8)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(jLabel7)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(tGaz, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel7))
                    .addComponent(jLabel8))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
		// -------------------------------------

		/*
		* В данном блоке задаются рамки для панели
		* Метод createTitledBorder задаёт заголовок для бордера
		*/
        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Капитальный ремонт", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(102, 102, 255))); // NOI18N
		// ------------------------------

        tOverhaul.setName("tOverhaul"); // NOI18N

        jLabel10.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel10.setText("м2");

		/*
		 * Создание объекта GroupLayout
		 * GroupLayout менеджер имеет возможность независимо устанавливать горизонтальное и вертикальное расположение компонентов на форме.
		 */
        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
		// Метод setLayout созланную панель в контейнер
        jPanel6.setLayout(jPanel6Layout);
		// Методы setHorizontalGroup и setVerticalGroup располагают элементы по осям x и y
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(tOverhaul, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel10)
                .addContainerGap(39, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tOverhaul, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10))
                .addGap(0, 0, Short.MAX_VALUE))
        );
		// --------------------------------------

		/*
		 * Создание объекта GroupLayout
		 * GroupLayout менеджер имеет возможность независимо устанавливать горизонтальное и вертикальное расположение компонентов на форме.
		 */
        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
		// Метод setLayout созланную панель в контейнер
        jPanel1.setLayout(jPanel1Layout);
		// Методы setHorizontalGroup и setVerticalGroup располагают элементы по осям x и y
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 146, Short.MAX_VALUE)
                            .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, 146, Short.MAX_VALUE))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, 189, Short.MAX_VALUE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(11, 11, 11)
                                .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, 54, Short.MAX_VALUE)
                    .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, 54, Short.MAX_VALUE)
                    .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
		// -----------------------------------

		/*
		 * Задание параметров для кнопки
		 * Метод setText устанавливет текст кнопки
		 * Метод setName устанавливет имя кнопки
		 * Метод addActionListener добавляет обработчик к кнопке
		 */
        bTariff.setText("Тарифы");
        bTariff.setName("bTariff"); // NOI18N
        bTariff.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bTariffActionPerformed(evt);
            }
        });

		/*
		 * Задание параметров для кнопки
		 * Метод setText устанавливет текст кнопки
		 * Метод setName устанавливет имя кнопки
		 * Метод addActionListener добавляет обработчик к кнопке
		 */
        bCalculate.setText("Расчитать");
        bCalculate.setName("bCalculate"); // NOI18N
        bCalculate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bCalculateActionPerformed(evt);
            }
        });

        lCalculate.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lCalculate.setText("Общая сумма затрат:");
        lCalculate.setName("lCalculate"); // NOI18N

		/*
		* В данном блоке задаются рамки для панели
		* Метод createTitledBorder задаёт заголовок для бордера
		*/
        jPanel8.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Результат", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 2, 14), new java.awt.Color(255, 153, 51))); // NOI18N
        jPanel8.setForeground(new java.awt.Color(255, 153, 102));
        jPanel8.setToolTipText("");
		// -----------------------------------

		// Задаётся пустая модель JTable
        tResult.setModel(new javax.swing.table.DefaultTableModel(
			// Строки
            new Object [][] {
                {}
            },
			// Колонки
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tResult);

		/*
		 * Создание объекта GroupLayout
		 * GroupLayout менеджер имеет возможность независимо устанавливать горизонтальное и вертикальное расположение компонентов на форме.
		 */
        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
		// Метод setLayout созланную панель в контейнер
        jPanel8.setLayout(jPanel8Layout);
		// Методы setHorizontalGroup и setVerticalGroup располагают элементы по осям x и y
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE)
                .addContainerGap())
        );

		/*
		 * Задание параметров для кнопки
		 * Метод setLabel устанавливет текст кнопки
		 * Метод addActionListener добавляет обработчик к кнопке
		 */
        bClear.setLabel("Очистить");
        bClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bClearActionPerformed(evt);
            }
        });
		// ---------------------------

		/*
		 * Создание объекта GroupLayout
		 * GroupLayout менеджер имеет возможность независимо устанавливать горизонтальное и вертикальное расположение компонентов на форме.
		 */
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
		// Метод setLayout созланную панель в контейнер
        getContentPane().setLayout(layout);
		// Методы setHorizontalGroup и setVerticalGroup располагают элементы по осям x и y
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(21, 21, 21)
                        .addComponent(lCalculate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bCalculate)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bClear))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel8, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(bTariff)
                                .addGap(0, 0, Short.MAX_VALUE)))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(bTariff)
                .addGap(15, 15, 15)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lCalculate)
                    .addComponent(bCalculate)
                    .addComponent(bClear))
                .addContainerGap())
        );
		// -----------------------------

		// Метод pack устанавливает такой минимальный размер контейнера, который достаточен для отображения всех компонентов.
        pack();
    }// </editor-fold>//GEN-END:initComponents

	// Обработка события расчёта стоимости комунальных услуг
    private void bCalculateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bCalculateActionPerformed
        
        // Инициализация модели
        PerformIndicators pi = new PerformIndicators();
        
        /* 
		 * С помощью set-в задаём значения свойств в модели Prices
		 * C помощью вызова статического метода genericNumeric, класса Validation проверяем
		 введённые значения с компонента JTextField
		 */
        pi.setWaterHot(Validation.genericNumeric(tWaterHot.getText()));
        pi.setWaterCold(Validation.genericNumeric(tWaterCold.getText()));
        pi.setElectric(Validation.genericNumeric(tElectric.getText()));
        pi.setGaz(Validation.genericNumeric(tGaz.getText()));
        pi.setKeep(Validation.genericNumeric(tKeep.getText()));
        pi.setOverhaul(Validation.genericNumeric(tOverhaul.getText()));
		// -----------------------------
        
        // Вывод результата в Label
        lCalculate.setText("Общая сумма затрат: " + String.valueOf(rc.calculate(pi)));
        
        SetResultTable();
    }//GEN-LAST:event_bCalculateActionPerformed

    // Метод SetResultTable вывводит результат вычислений в таблицу tResult
    private void SetResultTable()
    {        
        // Инициализация базовой модели для таблицы
        DefaultTableModel table = new DefaultTableModel();
        
        // Добавление колонок
        table.addColumn("Наименование");        
        table.addColumn("Сумма");

        // Заполнение строк таблицы
        table.addRow(new Object[] {"Горячая вода", rc.getWaterHot()});
        table.addRow(new Object[] {"Холодная вода", rc.getWaterCold()});
        table.addRow(new Object[] {"Электричество", rc.getElectric()});
        table.addRow(new Object[] {"Газ", rc.getGaz()});
        table.addRow(new Object[] {"Текущий ремонт", rc.getKeep()});
        table.addRow(new Object[] {"Капитальный ремонт", rc.getOverhaul()});
        
        // Скармливаем нашеё талице модельку
        tResult.setModel(table);
    }
    
	// Обработка события для вывода формы прайсов
    private void bTariffActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bTariffActionPerformed
        // Вывод формы прайсов
        TariffForm tf = new TariffForm(prices);
        tf.show();
    }//GEN-LAST:event_bTariffActionPerformed

    // Обработка события очистки полей
    private void bClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bClearActionPerformed
        // Очистка полей
        tWaterHot.setText("");
        tWaterCold.setText("");
        tElectric.setText("");
        tGaz.setText("");
        tKeep.setText("");
        tOverhaul.setText("");
        
        // Преобразуем в базовую модель
        DefaultTableModel table = (DefaultTableModel)tResult.getModel();
        // Очистка полей таблицы
        table.setRowCount(0);
    }//GEN-LAST:event_bClearActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        
		/* Блок try catch существует для перехвата исключений
		* В данный записывается код который может вызвать исключение
		*/
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        // Создание и вывод формы
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                // Делаем форму видимой
                new MainForm().setVisible(true);
            }
        });
    }

    // Объявление переменных для компонентов формы
	// Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bCalculate;
    private javax.swing.JButton bClear;
    private javax.swing.JButton bTariff;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lCalculate;
    private javax.swing.JTextField tElectric;
    private javax.swing.JTextField tGaz;
    private javax.swing.JTextField tKeep;
    private javax.swing.JTextField tOverhaul;
    private javax.swing.JTable tResult;
    private javax.swing.JTextField tWaterCold;
    private javax.swing.JTextField tWaterHot;
    // End of variables declaration//GEN-END:variables
}
