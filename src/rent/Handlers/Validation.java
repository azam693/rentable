/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rent.Handlers;

/**
 * Класс валидации проверяет и конвертирует информацию
 * @author пк
 */
public class Validation 
{
    private static String defaultValue = "0";
    
    // Преобразование строки в число, если строка не содержит цифры, то ставится 0
    public static float genericNumeric(String str)
    {
        return Float.parseFloat(!str.isEmpty() && isNumeric(str) ? str : defaultValue);
    }
    
    // Проверка строки с помощью регулярного выражаения, является ли оно целым или вещественным числом
    public static boolean isNumeric(String str)
    {
        return str.matches("-?\\d+(\\.\\d+)?");
    }
}
