// Указывает пакет "pack", в котором находится файл класса
package rent.Handlers;

// Импортируются все классы из пространства пакета Models
import rent.Models.*;

/*	
 * public - модификатор доступа, отвечающий за доступ, члены класса доступны всем
   В данном случае он означает что класс TariffForm виден и доступен любому другому классу.
   Зарезервированное слово class означает описание класса MainForm.
   Данный класс расчитывает стоимость комунальных
*/
public class RentCalculate
{
    // Объявление закрытой переменной prices
    private Prices prices;
    
    // Объявление закрытых переменных
    private float cWaterHot = 0,
                    cWaterCold = 0,
                    cElectric = 0, 
                    cGaz = 0,
                    cKeep = 0,
                    cOverhaul = 0;       
    
    // Получаем  в конструкторе в качестве аргумента модель
    public RentCalculate(Prices prices)
    {
        // Присвоение объекта прайсов локальной переменной
        this.prices = prices;
    }
    
    // Суммируем оплату
    public float calculate(PerformIndicators pi)
    {
        // Вызов методов расчёта, данные поставляются из модели
        return calcWaterHot(pi.getWaterHot()) + 
                calcWaterCold(pi.getWaterCold()) +
                calcElectric(pi.getElectric()) + 
                calcGaz(pi.getGaz()) + 
                calcKeep(pi.getKeep()) + 
                calcOverhaul(pi.getOverhaul());
    }
    
    /*
     * Умножаем показание счётчиков на цену по тарифам
     * модификатор доступа private означает, что доступ к данному методу имеют
     * члены класса
     */
    private float calcWaterHot(float wHot)
    {
        cWaterHot = wHot * prices.getPriceWaterHot();
        return cWaterHot;
    }
    
    private float calcWaterCold(float wCold)
    {
        cWaterCold = wCold * prices.getPriceWaterCold();
        return cWaterCold;
    }
    
    private float calcElectric(float electric)
    {
        cElectric = electric * prices.getPriceElectric();
        return cElectric;
    }
    
    private float calcGaz(float gaz)
    {
        cGaz = gaz * prices.getPriceGaz();
        return cGaz;
    }
    
    private float calcKeep(float keep)
    {
        cKeep = keep * prices.getPriceKeep();
        return cKeep;
    }
    
    private float calcOverhaul(float overhaul)
    {
        cOverhaul = overhaul * prices.getPriceOverhaul();
        return cOverhaul;
    }
    // ---------------
    
    // Выозвращаем расчёты для каждого тарифа
    public float getWaterHot()
    {
        return cWaterHot;
    }
    
    public float getWaterCold()
    {
        return cWaterCold;
    }
    
    public float getElectric()
    {
        return cElectric;
    }
    
    public float getGaz()
    {
        return cGaz;
    }
    
    public float getKeep()
    {
        return cKeep;
    }
    
    public float getOverhaul()
    {
        return cOverhaul;
    }
    // -----------------
}
